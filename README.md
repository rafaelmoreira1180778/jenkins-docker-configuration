# Jenkins Docker Image + Docker-Compose Stack

This repository contains the Docker Image and Docker-Compose stack necessary to run the latest Jenkins version locally inside a Docker Container.

The image inside `docker/Dockerfile` was built to be used in ODSOFT, a curricular subject of Master in Computer Science - Software Engineering at [ISEP](https://www.isep.ipp.pt/).

Image present at [Docker Hub](https://hub.docker.com/r/rafaelmoreiraskrey/jenkins-docker-configuration), feel free to use it.

## Image Description

This image includes the installation of [Graphviz](https://graphviz.org/) that is necessary to generate some [PlantUML](https://plantuml.com/) diagrams upon running a gradle task such as `javadoc`.

In case more dependencies are needed they will be added to this image but if you have some suggestions please email me at 1180778@isep.ipp.pt and I'd be glad to add the dependencies.

## How to Use

Run `docker-compose up -d` on the root of this repository and either access it via `http://localhost:9005` (check and change this port on the `docker-compose.yml` if you need) or via a custom reverse-proxy configuration.

In the folder `nginx` there is a `.conf` file with the example configuration for NGINX Reverse-Proxy configuration, in case you need or want to use it. Adjust your DNS settings accordingly (e.g. in your `/etc/hosts` file for local installations).

If the image does not start right away this might be due to fle permissions. The data is set to be persistend on the Host Machine on the root of this repository inside `data/jenkins` so the necessary permissions are as follow:

`sudo chown -R 1000:1000 data/jenkins`

This way you will be setting the correct UID outside of the container as the owner of the folder and then you can just run:

`docker restart jenkins` or `docker-compose restart` to restart the container and everything will be up and running!

This Recording will get you up and running:

<a href="https://asciinema.org/a/TtUfpjSYndbUmRz5bkUXl8Swx" target="_blank" ><img src="https://asciinema.org/a/TtUfpjSYndbUmRz5bkUXl8Swx.svg" width="600"/></a>

## Usefull Commands:

To build the image before starting the docker-compose stack simply run `docker-compose build` and the image will be built for you. In this process you do not have to input anything.

### File Structure

```
.
├── docker
│   └── Dockerfile
├── docker-compose.yml
├── nginx
│   └── jenkins.conf
└── README.md
```